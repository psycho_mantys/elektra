# Anotações #

Algumas anotações gerais

### Fazer projeto com react-native-webview ###

* npm install react-native
* npx react-native init PROJECTNAME
* cd PROJECTNAME
* npm install
* npx react-native install react-native-webview

Depois seria normal, mas no meu caso tive que alterar `android/build.gradle` porque tem um bug na versão mais nova da biblioteca.

Também é nescessario instalar os SDK do android e configurar o arquivo que diz onde esta o SDK( `android/build.gradle` normalmente aqui). Essas coisas são feitas de forma automativa se vc usar o android-studio.

### Gerar apk do android ###

* npx react-native run-android --variant=release

Tem um procedimento parecido para o IOS que eu não testei.

